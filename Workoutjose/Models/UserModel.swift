//
//  UserModel.swift
//  Workoutjose
//
//  Created by MacBook on 02.01.21.
//

import SwiftUI

struct User: Identifiable {
    var name: String
    var height: Double
    var weight: Double
    var gender: String
    var age: Int

    var imageName: String?
    
    let id = UUID()
    
    var image: Image? {	
        if let imageName = imageName {
            return Image(imageName)
        }
        return nil
    }
}

func bmiCalc(height: Double, weight: Double) -> Double {  
    let bmi: Double = (weight)/((height*height)/10000)
    return Double(round(100*bmi)/100)
}


