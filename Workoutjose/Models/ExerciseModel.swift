//
//  ExerciseSet.swift
//  Workoutjose
//
//  Created by MacBook on 02.01.21.
//

import Foundation
import SwiftUI
import CoreLocation

struct ExerciseModel: Hashable, Codable, Identifiable , CustomStringConvertible {
    var id: Int
    var name: String
    var link: String
    
    var description: String
    var isFavorite: Bool
    
    var category: Category
    enum Category: String, CaseIterable, Codable {
        case favourites = "My Exercises"
        case all = "All Exercises"
        
    }
    
    private var imageName: String
    var image: Image {
        Image(imageName)
    }
    
    
}
