//
//  Step.swift
//  Workoutjose
//
//  Created by Murad Bayramli on 27.01.21.
//

import Foundation
import SwiftUI


struct HealthKitDataModel: Identifiable {
    let id = UUID()
    let title: String
    let currentData: Int
    let date: Date
    let color: Color
}


