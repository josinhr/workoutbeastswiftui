//
//  EditProfileView.swift
//  Workoutjose
//
//  Created by MacBook on 02.01.21.
//

import SwiftUI

class EditProfileMV: ObservableObject {
    
    @Published  var sampleUser = User(name: "Murad", height: 181.0, weight: 80.0, gender: "Male", age: 20, imageName: "murad")
    
    var name : String{
        get{return sampleUser.name}
        set{sampleUser.name = newValue}
    }
    

    var age : Int{
        get{return sampleUser.age}
        set{ sampleUser.age = newValue}

    }
    
    var height : Double{
        get{return sampleUser.height}
        set{sampleUser.height = newValue}
    }
    
    var weight : Double {
        get{return  sampleUser.weight}
        set{sampleUser.weight = newValue}
    }
    
    
    var gender : String {
        get{return sampleUser.gender}
        set{sampleUser.gender = newValue}
    }
    
    var imageName : String{
        get{return sampleUser.imageName! }
        set{sampleUser.imageName!=newValue}
    }
    
}
