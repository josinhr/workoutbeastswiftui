//
//  Color+Extension.swift
//  Workoutjose
//
//  Created by Alisafa Gasimov  on 1/24/21.
//

import SwiftUI

extension Color {
    
    static let darkBlue=Color( red: 0.42, green: 0.36, blue: 0.91, opacity: 1.0)
}
