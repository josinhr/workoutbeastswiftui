//
//  ButtonView.swift
//  Workoutjose
//
//  Created by MacBook on 02.01.21.
//

import SwiftUI

struct EditButtonView: View {
    var user:User
    
    var body: some View {
        NavigationLink (
            destination: EditProfileView(user: user),
            label: {Text("Edit profile").font(.title3).padding(.vertical)})
            
    }
}

struct ButtonView_Previews: PreviewProvider {
    static var previews: some View {
        EditButtonView(user: sampleUser[0])
    }
}
