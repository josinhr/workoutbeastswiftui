//
//  HealthStore.swift
//  Workoutjose
//
//  Created by Murad Bayramli on 26.01.21.
//

import SwiftUI
import HealthKit

extension Date {
    static func mondayAt12AM() -> Date {
        return Calendar(identifier: .iso8601).date(from: Calendar(identifier: .iso8601).dateComponents([.yearForWeekOfYear, .weekOfYear], from: Date()))!
    }
}

class HealthStore {
    
    var healthStore: HKHealthStore?
    var query : HKStatisticsCollectionQuery?
    
    init(){
        if HKHealthStore.isHealthDataAvailable(){
            healthStore = HKHealthStore()
        }
    }
    
    func calculateStatistic (completion: @escaping (HKStatisticsCollection?) -> Void){
        
        let stepType = HKQuantityType.quantityType(forIdentifier: HKQuantityTypeIdentifier.stepCount)!
        let caloriesType = HKQuantityType.quantityType(forIdentifier: HKQuantityTypeIdentifier.activeEnergyBurned)!
        let waterType = HKQuantityType.quantityType(forIdentifier: HKQuantityTypeIdentifier.dietaryWater)!
        let identifiers = [caloriesType, stepType, waterType]
        
        for id in identifiers {
            let startDate = Calendar.current.date(byAdding: .day, value: -7, to: Date())
            
            let anchorDate = Date.mondayAt12AM()
            let daily = DateComponents(day: 1)
            
            let predicate = HKQuery.predicateForSamples(withStart: startDate, end: Date(), options: .strictStartDate)
            
            query = HKStatisticsCollectionQuery(quantityType: id, quantitySamplePredicate: predicate, options: .cumulativeSum, anchorDate: anchorDate, intervalComponents: daily)
            
            query!.initialResultsHandler = {
                query, statisticsCollection, error in
                completion(statisticsCollection)
            }
            
            if let healthStore = healthStore, let query = self.query {
                healthStore.execute(query)
            }
        }
        
    }
    
    func requestAuthorization(completion: @escaping (Bool) -> Void){
        
        let stepType = HKQuantityType.quantityType(forIdentifier: HKQuantityTypeIdentifier.stepCount)!
        let heartRateType = HKQuantityType.quantityType(forIdentifier: HKQuantityTypeIdentifier.heartRate)!
        let caloriesType = HKQuantityType.quantityType(forIdentifier: HKQuantityTypeIdentifier.activeEnergyBurned)!
        let waterType = HKQuantityType.quantityType(forIdentifier: HKQuantityTypeIdentifier.dietaryWater)!
        let sleepType = HKObjectType.categoryType(forIdentifier: HKCategoryTypeIdentifier.sleepAnalysis)!
        
        
        guard let healthStore = self.healthStore else {return completion(false)}
        
        healthStore.requestAuthorization(toShare: [], read: [stepType, caloriesType, waterType, heartRateType, sleepType]) { (success, error) in
            
            completion(success)
            
        }
    }
    
}
