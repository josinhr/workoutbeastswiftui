//
//  ControlsButtons.swift
//  Workoutjose
//
//  Created by MacBook on 02.02.21.
//

import SwiftUI
import AVFoundation
import AVKit

struct ControlsButtonsView: View {
    @ObservedObject var stopWatchManager :StopWatchManager
    
    init(manager:StopWatchManager) {
        stopWatchManager = manager
        
    }
    
    var body: some View {
        VStack{
            Spacer()
            
            if stopWatchManager.mode == .stopped {
                Button(action: {self.stopWatchManager.start()}){
                    FloatingMenu(image: "pause.fill", height: UIScreen.main.bounds.height*1/21, width: UIScreen.main.bounds.height*1/21, system: true).padding(UIScreen.main.bounds.height*1/20)
                }.transition(.opacity)
            }
            
            if stopWatchManager.mode == .running {
                Button(action: {self.stopWatchManager.pause()}){
                    FloatingMenu(image: "pause.fill", height: UIScreen.main.bounds.height*1/21, width: UIScreen.main.bounds.height*1/21, system: true).padding(UIScreen.main.bounds.height*1/20)
                }.transition(.opacity)
            }
            
            if stopWatchManager.mode == .paused {
                HStack{Button(action: {self.stopWatchManager.start()}){
                    FloatingMenu(image: "play.fill", height: UIScreen.main.bounds.height*1/21, width: UIScreen.main.bounds.height*1/21, system: true).padding(UIScreen.main.bounds.height*1/20)
                }.transition(.opacity)
                NavigationLink(
                    destination: MainHomeView()
                ){
                    FloatingMenu(image: "stop.fill", height: UIScreen.main.bounds.height*1/21, width: UIScreen.main.bounds.height*1/21, system: true).padding(UIScreen.main.bounds.height*1/20)
                }
                .transition(.opacity)}
                
            }
        }
    }
    
}

class StopWatchManager: ObservableObject{
    @Published var secondsElapse:Int
    @Published var mode: stopWatchMode = .running
    @ObservedObject var player: PlayerUIView
    
    init(time:Int,player:PlayerUIView) {
        secondsElapse = time
        self.player = player
        start()
    }
    
    var timer = Timer()
    enum stopWatchMode{
        case running
        case stopped
        case paused
    }
    
    func start() {
        mode = .running
        timer = Timer()
        timer = Timer.scheduledTimer(withTimeInterval: 1.0, repeats: true){ (Timer) in
            if self.secondsElapse > 0 {
                self.secondsElapse -= 1
            } else {
                Timer.invalidate()
            }
        }
        player.player.play()
        
    }
    func stop(){
        timer.invalidate()
        secondsElapse = 0
        mode = .stopped
    }
    
    func pause(){
        timer.invalidate()
        mode = .paused
        player.player.pause()
    }
    
    @objc func updateCounter() {
        if secondsElapse > 0 {
            secondsElapse -= 1
        }
    }
}
