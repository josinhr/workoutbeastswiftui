//
//  PerformRoutineView.swift
//  Workoutjose
//
//  Created by MacBook on 26.01.21.
//

import SwiftUI
import AVFoundation
import AVKit


struct BeforePerformRoutineView: View {
    @EnvironmentObject var exerciseSetModel: ExerciseSetModel
    var routine: RoutineModel
    @State private var pulsate = false
    @State private var showWaves = false
    @State private var startCountDown = true
    @State var counter: Int = 0
    @State var finishCounter: Bool = false
    var countTo: Int = 3
    
    var body: some View {
        ZStack{
            WebVideoUnresizedView(player:PlayerUIView(frame: .zero, videoLink: exerciseSetModel.exerciseSets[ Int.random(in:0..<exerciseSetModel.exerciseSets.count)].link))
            
            
            
            if(!startCountDown){
                VStack(spacing:100){
                    ZStack{
                        Circle()
                            .stroke(lineWidth: 2)
                            .frame(width: /*@START_MENU_TOKEN@*/100/*@END_MENU_TOKEN@*/, height: /*@START_MENU_TOKEN@*/100/*@END_MENU_TOKEN@*/)
                            .foregroundColor(.darkBlue)
                            .scaleEffect(showWaves ? 2:1)
                            .hueRotation(.degrees(showWaves ? 360 :0))
                            .opacity(showWaves ? 0:1)
                            .animation(Animation.easeInOut(duration: 2).repeatForever( autoreverses: false).speed(1))
                            .onAppear(){
                                self.showWaves.toggle()
                            }
                        Circle()
                            .frame(width: /*@START_MENU_TOKEN@*/100/*@END_MENU_TOKEN@*/, height: /*@START_MENU_TOKEN@*/100/*@END_MENU_TOKEN@*/)
                            .foregroundColor(.darkBlue)
                            .scaleEffect(pulsate ? 1: 1.2)
                            .animation(Animation.easeInOut(duration: 1).repeatForever(autoreverses: true).speed(1))
                            .onAppear(){
                                self.pulsate.toggle()
                            }
                        Button(action: {self.startCountDown.toggle()}){
                            
                            Image(systemName: "play.fill")
                                .font(.largeTitle)
                                .foregroundColor(.white)
                        }
                    }
                    NavigationLink(
                        destination: MainHomeView()
                    ){
                        FloatingMenu(image: "arrow.backward",height: 35,width: 35, system: true)
                    }}
                
            }
            if(startCountDown){
                CountdownView(countTo: 3).onAppear(perform: {
                    
                    
                    
                    DispatchQueue.main.asyncAfter(deadline: .now() + 3) {
                        
                        finishCounter.toggle()
                    }
                })
                .transition(AnyTransition.move(edge: .top).animation(.easeInOut(duration: 4.0)))
                NavigationLink(destination: PerformRoutineView(data:routine), isActive: $finishCounter){
                    EmptyView()
                }
            }
            
        }.shadow(radius:25 ).frame(width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height+100)
        .navigationBarTitle("")
        .navigationBarHidden(true)
        .navigationBarBackButtonHidden(true)
    }
}





struct BeforePerformRoutineView_Previews: PreviewProvider {
    
    static let modelData = ExerciseSetModel()
    static let exerciseTime = [
        ExerciseTime(id: 1, time: 30, exercise: modelData.exerciseSets[0]),
        ExerciseTime(id: 2, time: 30, exercise: modelData.exerciseSets[1]),
        ExerciseTime(id: 3, time: 30, exercise: modelData.exerciseSets[2]),
        ExerciseTime(id: 4, time: 30, exercise: modelData.exerciseSets[3])]
    
    
    
    static var previews: some View {
        let routine = RoutineModel(id: 1, date: Date(), exerciseTime:exerciseTime)
        
        BeforePerformRoutineView(routine: routine)
            .environmentObject(ExerciseSetModel())
    }
}
