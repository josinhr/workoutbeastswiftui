//
//  ExerciseInformationView.swift
//  Workoutjose
//
//  Created by MacBook on 02.02.21.
//

import SwiftUI

struct ExerciseInformationView: View {
    var exercise:ExerciseTime
    
    var body: some View {
        ZStack {
            RoundedRectangle(cornerRadius: 25, style: .continuous)
                .fill(Color.white)
                .shadow(radius: 10)
            
            HStack {
                
                
                Text( exercise.exercise.name)
                    .font(.body)
                    .foregroundColor(.gray)
                Divider().padding()
                Text( String(exercise.time) + " seconds")
                    .font(.body)
                    .foregroundColor(.gray)
                
            }
            
            .multilineTextAlignment(.center)
        }.frame( height: UIScreen.main.bounds.height*1/14)
    }
}
