//
//  PerformRoutineView.swift
//  Workoutjose
//
//  Created by MacBook on 26.01.21.
//
import SwiftUI
import AVFoundation
import AVKit

struct PerformRoutineView: View {
    
    var data : RoutineModel
    @State var playedExercise: Int = 0
    func getTotalRoutineTime() -> Int {
        return data.exerciseTime.reduce(0, { $0 + $1.time })
    }
    @State var finishRoutine: Bool = false
    
    var countTo: Int { return getTotalRoutineTime()}
    @EnvironmentObject var exerciseSetModel: ExerciseSetModel
    static let modelData = ExerciseSetModel()
    @ObservedObject var stopWatchManager :StopWatchManager
    @State var audioPlayer: AVAudioPlayer!
    
    init(data: RoutineModel) {
        self.data = data
        self.stopWatchManager = StopWatchManager(time: data.exerciseTime[0].time, player: PlayerUIView(frame: .zero, videoLink:data.exerciseTime[0].exercise.link))
       
        var array:[ExerciseTime] = []
        let rest:ExerciseModel = PerformRoutineView.modelData.exerciseSets.filter{
            return $0.name == "Rest"
        }[0]
        let timeRest: ExerciseTime = ExerciseTime(id: 9999, time: 5, exercise: rest)
        
        for i in 0...self.data.exerciseTime.count-1{
            array.append(self.data.exerciseTime[i])
            array.append(timeRest)
        }
        self.data.exerciseTime = array
    }
    
    var body: some View {
        ZStack{
            
            ForEach(data.exerciseTime.indices){i in
                
                ZStack{
                    if i == self.playedExercise{
                        ZStack{
                            
                            VStack{
                                if(i+1<self.data.exerciseTime.count) {
                                    NextExerciseView(data: data, i: i)
                                }
                                Spacer()
                            }.padding(.top, UIScreen.main.bounds.height*1/18)
                            
                            VStack( spacing:0){
                                
                                ZStack{
                                    WebVideoView(player:stopWatchManager.player)
                                        .shadow(radius: 10)
                                    if(stopWatchManager.secondsElapse==0){
                                        Text("NEXT").onAppear(perform: {
                                                                let newShow:Int = playedExercise + 1
                                                                if newShow == self.data.exerciseTime.count{
                                                                    finishRoutine.toggle()}
                                                                else{
                                                                    self.stopWatchManager.pause()
                                                                    self.stopWatchManager.secondsElapse = self.data.exerciseTime[newShow].time
                                                                    
                                                                    self.stopWatchManager.start()
                                                                    self.stopWatchManager.player=PlayerUIView(frame: .zero, videoLink:self.data.exerciseTime[newShow].exercise.link)
                                                                    
                                                                    
                                                                    withAnimation{
                                                                        self.playedExercise = newShow % self.data.exerciseTime.count
                                                                    }
                                                                }})
                                    }
                                }
                                ExerciseInformationView(exercise: self.data.exerciseTime[i]).padding()
                                CountDownWithManager(stopWatchManager: stopWatchManager,exerciseTime: self.data.exerciseTime[i].time)
                                
                                
                            }
                            ControlsButtonsView(manager:stopWatchManager)
                        }
                        
                        .transition(AnyTransition.move(edge: .top).animation(.easeInOut(duration: 4.0)))
                        .frame(width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
                        .onAppear{
                            if(i == 0){
                            DispatchQueue.main.asyncAfter(deadline: .now() +
                                                            Double(self.data.exerciseTime[i].time-6)) {
                                      
                            let sound = Bundle.main.path(forResource: "routine", ofType: "wav")
                            self.audioPlayer = try! AVAudioPlayer(contentsOf: URL(fileURLWithPath: sound!))
                            self.audioPlayer.play()
                               
                            }
                        }
                            else{
                                DispatchQueue.main.asyncAfter(deadline: .now() +
                                                                Double(self.data.exerciseTime[i].time-3)) {
                                          
                                let sound = Bundle.main.path(forResource: "routine", ofType: "wav")
                                self.audioPlayer = try! AVAudioPlayer(contentsOf: URL(fileURLWithPath: sound!))
                                self.audioPlayer.play()
                                   
                                }
                            }
                        }
                        .gesture(DragGesture()
                                    .onEnded { _ in
                                        let newShow:Int = playedExercise + 1
                                        if newShow == self.data.exerciseTime.count{
                                            finishRoutine.toggle()}
                                        else{
                                            self.stopWatchManager.pause()
                                            self.stopWatchManager.secondsElapse = self.data.exerciseTime[newShow].time
                                            self.stopWatchManager.start()
                                            self.stopWatchManager.player=PlayerUIView(frame: .zero, videoLink:self.data.exerciseTime[newShow].exercise.link)
                                            
                                            
                                            withAnimation{
                                                self.playedExercise = newShow % self.data.exerciseTime.count
                                            }}
                                    })
                    }
                }
                NavigationLink(destination: EndRoutineView(routine: data), isActive: $finishRoutine){
                    EmptyView()
                }
            }
            
        }
        .frame(width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height+100).background(Color.black.opacity(0.2))
        .navigationBarTitle("")
        .navigationBarHidden(true)
        .navigationBarBackButtonHidden(true)
    }
    
   
}

struct TryView_Previews: PreviewProvider {
    static let modelData = ExerciseSetModel()
    static let exerciseTime = [
        ExerciseTime(id: 1, time: 30, exercise: modelData.exerciseSets[0]),
        ExerciseTime(id: 2, time: 30, exercise: modelData.exerciseSets[1]),
        ExerciseTime(id: 3, time: 30, exercise: modelData.exerciseSets[2]),
        ExerciseTime(id: 4, time: 30, exercise: modelData.exerciseSets[3])]
    
    
    
    static var previews: some View {
        let routine = RoutineModel(id: 1, date: Date(), exerciseTime:exerciseTime)
        
        PerformRoutineView(data: routine)
            .environmentObject(ExerciseSetModel())
    }
}


