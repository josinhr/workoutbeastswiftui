//
//  CountDownWithManager.swift
//  Workoutjose
//
//  Created by MacBook on 02.02.21.
//

import SwiftUI

struct CountDownWithManager: View {
    
    @ObservedObject var stopWatchManager :StopWatchManager
    var exerciseTime: Int
    
    var body: some View {
        VStack{
            ZStack{
                ProgressTrack().shadow(radius: 10)
                ProgressBar(counter: exerciseTime-stopWatchManager.secondsElapse, countTo: exerciseTime).shadow(radius: 10)
                Clock(counter: exerciseTime-stopWatchManager.secondsElapse, countTo: exerciseTime).shadow(radius: 10)
            }
        }
    }
}


