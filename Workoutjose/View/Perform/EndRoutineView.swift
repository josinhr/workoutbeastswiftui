//
//  EndRoutineView.swift
//  Workoutjose
//
//  Created by MacBook on 03.02.21.
//

import SwiftUI

import SwiftUI

struct EndRoutineView: View {
    @EnvironmentObject var exerciseSetModel: ExerciseSetModel
    var routine: RoutineModel
    
    var body: some View {
        ZStack{
            WebVideoUnresizedView(player:PlayerUIView(frame: .zero, videoLink: exerciseSetModel.exerciseSets[ Int.random(in:0..<exerciseSetModel.exerciseSets.count)].link))
            
            VStack() {
                
                
                Text("Great job!").foregroundColor(.black).bold().font(.largeTitle)
                NavigationLink(destination: MainHomeView()) {
                    ZStack{
                        Circle()
                            .fill(Color.darkBlue)
                            .frame(width: 150, height: 150)
                        Text("Home").bold().foregroundColor(.white).font(.title)
                        
                    } .shadow(radius: 15)
                }
                
                
                
            }.padding()
            
            
        }.frame(width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height+100).navigationBarTitle("")
        .navigationBarHidden(true)
        .navigationBarBackButtonHidden(true)
    }
    func getDate (val: Date) -> String {
        let formatter1 = DateFormatter()
        formatter1.dateStyle = .short
        return formatter1.string(from: val)
    }
}



struct EndRoutineView_Previews: PreviewProvider {
    
    static let modelData = ExerciseSetModel()
    static let exerciseTime = [
        ExerciseTime(id: 1, time: 30, exercise: modelData.exerciseSets[0]),
        ExerciseTime(id: 2, time: 30, exercise: modelData.exerciseSets[1]),
        ExerciseTime(id: 3, time: 30, exercise: modelData.exerciseSets[2]),
        ExerciseTime(id: 4, time: 30, exercise: modelData.exerciseSets[3])]
    static var previews: some View {
        let routine = RoutineModel(id: 1, date: Date(), exerciseTime:exerciseTime)
        
        EndRoutineView(routine: routine)
            .environmentObject(ExerciseSetModel())
    }
}


