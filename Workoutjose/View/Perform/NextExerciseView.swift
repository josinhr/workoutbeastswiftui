//
//  NextExerciseView.swift
//  Workoutjose
//
//  Created by MacBook on 30.01.21.
//

import SwiftUI

struct NextExerciseView: View {
    var data : RoutineModel
    var i: Int
    
    var body: some View {
        ZStack{
            
            RoundedRectangle(cornerRadius: 25, style: .continuous)
                .fill(Color.white)
                .frame(width: UIScreen.main.bounds.width*0.7, height: UIScreen.main.bounds.height*1/15).shadow(radius: 10)
            
            
            HStack(alignment: .center){
                Text("Next exercise: ")
                
                Text( self.data.exerciseTime[i+1].exercise.name)
                    
                    .font(.title)
                    .foregroundColor(Color.darkBlue)
            }
        }
    }
}


