//
//  RoutineModel.swift
//  Workoutjose
//
//  Created by MacBook on 26.01.21.
//

import Foundation

struct RoutineModel: Codable,Identifiable {
    let id: Int
    let date: Date
    var exerciseTime: [ExerciseTime]


}
struct ExerciseTime: Codable, Identifiable {
    let id: Int
    let time: Int
    let exercise: ExerciseModel
}
