//
//  EditProfileView.swift
//  Workoutjose
//
//  Created by MacBook on 02.01.21.
//


import SwiftUI

struct EditProfileDataView: View {
    @State private var lines = ["Gender: ", "Date of Birth: ", "Weight: ", "Height: ", "Target Weight: "]
    @StateObject var user = EditProfileMV()
    @State private var showSheet: Bool = false
    @State private var showImagePicker: Bool = false
    @State private var sourceType: UIImagePickerController.SourceType = .camera
    @State private var image: UIImage?
    
    var body: some View {
        
        
        VStack {
            ScrollView(){
                VStack {
                    Header()
                        .foregroundColor(Color.darkBlue)
                        .edgesIgnoringSafeArea(.top)
                        .frame(height: 200)
                        .overlay(Image("gym3").resizable().aspectRatio(contentMode: .fill))
                    
                    
                    (image == nil ? Image(user.imageName):Image(uiImage: image!))
                        .resizable()
                        .aspectRatio(contentMode: .fit)
                        .clipShape(Circle())
                        .overlay(Circle().stroke(Color.white, lineWidth: 4))
                        .shadow(radius: 40)
                        .offset(y: -160)
                        .padding(.bottom, -230)
                        .frame(width: 200, height: 100)
                    
                    
                    Spacer(minLength: 60)
                    
                    
                    VStack {
                        
                        Button("Change picture"){
                            self.showSheet = true
                        }
                        .actionSheet(isPresented: $showSheet){
                            ActionSheet(title: Text("Select Photo"), message: Text("Choose"), buttons: [
                                .default(Text("Photo  Library")){
                                    self.showImagePicker = true
                                    self.sourceType = .photoLibrary
                                },
                                .default(Text("Open Camera")){
                                    self.showImagePicker = true
                                    self.sourceType = .camera
                                },
                                .cancel()
                            ])
                        }
                        
                    }.sheet(isPresented: $showImagePicker){
                        ImagePicker(image: self.$image, isShown: self.$showImagePicker, sourceType: self.sourceType)
                        
    
                    }.padding()
                    
                    Spacer(minLength: 28)
                    
                    Text(user.name)
                        .bold()
                        .font(.title)
                    
                    
                    Divider()
                    
                    Spacer()
                    
                    VStack{
                        
                        
                        RoundedRectangle(cornerRadius: 50.0)
                            .frame(width: 360, height: 70)
                            .foregroundColor(Color(red: 0.42, green: 0.36, blue: 0.91, opacity: 1.0))
                            .overlay(Text("Name:     ").foregroundColor(.white).padding(.trailing, 100.0))
                            .overlay(TextField("Name:",text:$user.name
                            ).foregroundColor(.white).padding(.leading, 250.0))
                            .padding()
                   
                        
                        RoundedRectangle(cornerRadius: 50.0)
                            .frame(width: 360, height: 70)
                            .foregroundColor(Color(red: 0.42, green: 0.36, blue: 0.91, opacity: 1.0))
                            .overlay(Text("Age:     ").foregroundColor(.white).padding(.trailing, 100.0))
                            .overlay(TextField("Age:" ,value:$user.age,formatter: NumberFormatter())
                            .foregroundColor(.white).padding(.leading, 250.0))
                            .padding(.horizontal)
                        
                        RoundedRectangle(cornerRadius: 50.0)
                            .frame(width: 360, height: 70)
                            .foregroundColor(Color(red: 0.42, green: 0.36, blue: 0.91, opacity: 1.0))
                            .overlay(Text("Gender:     ").foregroundColor(.white).padding(.trailing, 100.0))
                            .overlay(TextField("Gender:" ,text:$user.gender)
                                        .foregroundColor(.white).padding(.leading, 250.0))
                            .padding()
                        
                        RoundedRectangle(cornerRadius: 50.0)
                            .frame(width: 360, height: 70)
                            .foregroundColor(Color(red: 0.42, green: 0.36, blue: 0.91, opacity: 1.0))
                            .overlay(Text("Weight:     ").foregroundColor(.white).padding(.trailing, 100.0))
                            .overlay(TextField("Weight" ,value:$user.weight,formatter: NumberFormatter())
                                        .foregroundColor(.white).padding(.leading, 250.0))
                            .padding(.horizontal)
                        
                        RoundedRectangle(cornerRadius: 50.0)
                            .frame(width: 360, height: 70)
                            .foregroundColor(Color(red: 0.42, green: 0.36, blue: 0.91, opacity: 1.0))
                            .overlay(Text("Height:     ").foregroundColor(.white).padding(.trailing, 100.0))
                            .overlay(TextField("Height:" ,value:$user.height,formatter: NumberFormatter())
                                        .foregroundColor(.white).padding(.leading, 250.0))
                            .padding([.top, .leading, .trailing])
                        
                        let bmi = bmiCalc(height: user.height, weight: user.weight)
                        
                        RoundedRectangle(cornerRadius: 50.0)
                            .frame(width: 360, height: 70)
                            .foregroundColor(Color(red: 0.42, green: 0.36, blue: 0.91, opacity: 1.0))
                            .overlay(Text("Mass Index:              " + "\(bmi)").foregroundColor(.white))
                            .padding([.top, .leading, .trailing])
                        
                        
                        
                        if bmi < 18.0 {
                            Text("Gain some mass!").foregroundColor(.red)
                                .padding(.all)
                        }
                        else if bmi < 25.0 && bmi > 18.0 {
                            Text("You got the perfect mass!").foregroundColor(.green)
                                .padding(.all)
                        }
                        else if bmi > 25.0 && bmi < 30.0 {
                            Text("You are overweighted!").foregroundColor(.blue)
                                .padding(.all)
                        }
                        else  {
                            Text("You are too fat! Start training now!").foregroundColor(.red)
                                .padding(.all)
                        }
                    }
                    .padding(.top)
                    
                    
                    
                }
                
                
                
            }
            
        }
        
    }
}








struct EditProfileView_Previews: PreviewProvider {
    static var previews: some View {
        EditProfileDataView()
    }
    
}





