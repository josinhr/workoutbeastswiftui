//
//  CategoryRowView.swift
//  Workoutjose
//
//  Created by MacBook on 02.01.21.
//
import SwiftUI

struct CategoryRowView: View {
    var categoryName: String
    var items: [ExerciseModel]
    
    var body: some View {
        VStack(alignment: .leading) {
            Text(categoryName)
                .font(.title2)
                .fontWeight(.bold)
                .foregroundColor(.darkBlue)
            ScrollView(.horizontal, showsIndicators: false) {
                HStack(alignment: .top, spacing: 15) {
                    ForEach(items) { exercise in
                        NavigationLink(destination: ExerciseDetail(exercise: exercise)) {
                            CategoryItem(exercise: exercise)
                        }
                    }
                }.padding( )
            }
            .frame(height: 230)
        }
    }
}
