//
//  CategoryRowView.swift
//  Workoutjose
//
//  Created by MacBook on 02.01.21.
//
import SwiftUI

struct CardView: View {
    @State private var translation: CGSize = .zero
    var name :String
    var image : Image
    var exerciseModelLocal : ExerciseModel
    
    var thresholdPercentage: CGFloat = 0.5
    @State  var isActive=false
    
    func getGesturePercentage(_ geometry: GeometryProxy, from gesture: DragGesture.Value) -> CGFloat {
        gesture.translation.width / geometry.size.width
    }
    
    var body: some View {
        
   
        GeometryReader { geometry in
            
           
            VStack(alignment: .leading) {
                image
                    .resizable()
                    .scaledToFill()
                    .frame(width: geometry.size.width, height: geometry.size.height * 0.8 )
                    .clipped()
                
                HStack {
                    Text(name)
                        .font(.title)
                        .bold()
                        .foregroundColor(.darkBlue)
                    Spacer()
                    NavigationLink(destination: ExerciseDetail(exercise: exerciseModelLocal) ,isActive: $isActive){
                        Button(action: {isActive=true})
                        {
                            Image(systemName: "info.circle")
                                .foregroundColor(.gray)
                            
                        }
                    }
                    
                }.padding(.horizontal)
                
            }
            .shadow(radius: 5)
            .offset(x: self.translation.width, y: self.translation.height)
            .gesture(
                
                DragGesture()
                    
                    .onChanged { value in
                        self.translation = value.translation
                    }.onEnded { value in
                    
                        if abs(self.getGesturePercentage(geometry, from: value)) > self.thresholdPercentage {
                           
                        } else {
                            self.translation = .zero
                        }
                        self.translation = .zero
                    }
            )
            
            .padding(.bottom)
            .background(Color.white)
            .cornerRadius(10)
            .shadow(radius: 5)
        }
    }
}


