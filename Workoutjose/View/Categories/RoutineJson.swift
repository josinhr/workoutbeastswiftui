//
//  Routine.swift
//  Workoutjose
//
//  Created by Alisafa Gasimov  on 2/6/21.
//

import Foundation

struct  RoutineJson :Codable{
    var startDate : String
    let  routineName : [String]
    let duration: [Double]
    let increase : [Int]
    let startAmount : [Int]
    let id : [Int]
}

