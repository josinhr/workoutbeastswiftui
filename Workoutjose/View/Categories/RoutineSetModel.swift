//
//  ExerciseSetModel.swift
//  Workoutjose
//
//  Created by MacBook on 02.01.21.
//

import Foundation
import Combine

final class RoutineSetModel : ObservableObject {
    
    
    @Published var routineSets :  RoutineJson
    @Published var routineModels :  [RoutineModel]
    
    
    init() {
        routineSets = loadRoutineSets()
        routineModels = []
        if(routineSets.routineName.count > 0){
            let formatter = DateFormatter()
            formatter.locale = Locale(identifier: "US_en")
            formatter.dateFormat = "yyyy-MM-dd HH:mm:ss Z"
            let startDate = formatter.date(from:routineSets.startDate)!
           
            for i in 0...routineSets.duration.count-1{
                
                for day in 0...Int(routineSets.duration[i])-1{
                    
                    let date = Calendar.current.date(byAdding:
                                                        .day,
                                                     value: day,
                                                     to: startDate)!
                    
                    
                    var index = 0
                    while (index < routineModels.count) && (routineModels[index].date != date)   {
                        index+=1
                    }
                    var routine : RoutineModel
                    if(index < routineModels.count){
                        routine = routineModels[index]
                        
                        let exercise: ExerciseModel = ExerciseSetModel().exerciseSets.filter{ return $0.name == routineSets.routineName[i] }[0]
                        let exerciseTime = ExerciseTime(id: exercise.id, time: Int(routineSets.duration[i])*5+day*routineSets.increase[i]*5, exercise: exercise)
                        routine.exerciseTime.append(exerciseTime)
                        routineModels[index] = routine
                    }
                    else{
                        routine = RoutineModel(id: Int.random(in: 0..<1000), date: date , exerciseTime: [])
                        let exercise: ExerciseModel = ExerciseSetModel().exerciseSets.filter{ return $0.name == routineSets.routineName[i] }[0]
                        
                        let exerciseTime = ExerciseTime(id: exercise.id, time: Int(routineSets.duration[i])*5+day*routineSets.increase[i]*5, exercise: exercise)
                        routine.exerciseTime.append(exerciseTime)
                        routineModels.append(routine)
                    }
                }
            }
        }
    }
}


func loadRoutineSets<T: Decodable>() -> T {
    
    let data: Data
    let selectedExercise = RoutineRetrieveModel()
    
    
    do {
        let file  = try FileManager.default.url(for: .documentDirectory, in: .userDomainMask,appropriateFor: nil, create: true).appendingPathComponent("routine.json")
       
        data = try Data(contentsOf: file)
        
    } catch {
        selectedExercise.writeToJson()
        do {
            let file  = try FileManager.default.url(for: .documentDirectory, in: .userDomainMask,appropriateFor: nil, create: true).appendingPathComponent("routine.json")
            data = try Data(contentsOf: file)
        } catch {
            fatalError("Couldn't load  from main bundle:\n\(error)")
        }
        
        
    }
    
    do {
        
        let decoder = JSONDecoder()
        
        return try decoder.decode(T.self, from: data)
        
    } catch {
        fatalError("Couldn't parse  as \(T.self):\n\(error)")
    }
}

