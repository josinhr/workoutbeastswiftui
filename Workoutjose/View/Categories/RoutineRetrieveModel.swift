import Combine
import SwiftUI

final class RoutineRetrieveModel: NSObject, ObservableObject  {
    
    @Published  var updateGlobalRoutineList  : [String: Any] = ["routineName" : [],"duration": [] ,"increase" : [] , "startAmount" : [],"startDate": "","id" : []]
    
    
    func writeToJson(){
        
        do {

                let fileURL = try FileManager.default
                    .url(for: .documentDirectory , in: .userDomainMask, appropriateFor: nil, create: true)
                    .appendingPathComponent("routine.json")

                try JSONSerialization.data(withJSONObject: updateGlobalRoutineList)
                    .write(to: fileURL)
           
            } catch {
                
            }

    }
}
