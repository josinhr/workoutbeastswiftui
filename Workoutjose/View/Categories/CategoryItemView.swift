//
//  CategoryItemView.swift
//  Workoutjose
//
//  Created by MacBook on 02.01.21.
//

import SwiftUI

struct CategoryItem: View {
    var exercise: ExerciseModel
    
    var body: some View {
        
        
        ZStack {
            RoundedRectangle(cornerRadius: 25, style: .continuous)
                .fill(Color.white)
                .shadow(radius: 10)
            
            VStack {
                exercise.image
                    .resizable()
                    .cornerRadius(5)
                
                Text(exercise.name)
                    .font(.headline)
                    .foregroundColor(.darkBlue)
            }
            .padding(20)
            .multilineTextAlignment(.center)
        }.frame(width: 200, height: 220)
      
    }
}

