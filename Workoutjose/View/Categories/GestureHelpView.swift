//
//  GestureHelpView.swift
//  Workoutjose
//
//  Created by Alisafa Gasimov  on 2/8/21.
//

import SwiftUI

struct GestureHelpView: View {
    var body: some View {
        VStack{
            HStack{
                Image("2xtap")
                Text("For Adding Exercises ")
                .fontWeight(.bold)
                .foregroundColor(.green)
                .cornerRadius(10)
                .shadow(radius: 5)}
            HStack{
                Image("drag")
                Text("For Skipping Exercises")
                .fontWeight(.bold)
                .foregroundColor(.red)
                .cornerRadius(10)
                .shadow(radius: 5)}
        }
    }
}

struct GestureHelpView_Previews: PreviewProvider {
    static var previews: some View {
        GestureHelpView()
    }
}
