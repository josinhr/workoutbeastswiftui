//
//  CategoryRowView.swift
//  Workoutjose
//
//  Created by MacBook on 02.01.21.
//
import SwiftUI


struct CategoriesColumnView: View {
    

    var categoryName: String = "All Exercises"
    var items: [ExerciseModel]
    @ObservedObject var modelDataObservable = ExerciseSetModel()
    @ObservedObject var selectedExercise :RoutineRetrieveModel
    

    @State private var tempRoutineName : [String] = []
    @State private var tempRoutineIncrease : [Int] = []
    @State private var tempRoutineStartAmount : [Int] = []
    @State private var tempRoutineDuration : [Double] = []
    @State private var tempRoutineInd : [Int] = []
    
 
    @State private var duration : Double = 1
    @State private var startAmount : Int = 0
    @State private var increaseDaily: Int = 0
    

    @State var isActive :Bool = false
    @State var isActiveHelp :Bool = false
    
    private func getCardWidth(_ geometry: GeometryProxy, id: Int) -> CGFloat {
        let offset: CGFloat = CGFloat(modelDataObservable.exerciseSets.count - 1 - id) * 10
        return geometry.size.width - offset
    }
    
    private func getCardOffset(_ geometry: GeometryProxy, id: Int) -> CGFloat {
        return  CGFloat(modelDataObservable.exerciseSets.count - 1 - id) * 5
    }
    
    var body: some View {
        VStack{
            HStack{
                
                Text("Choose own routine")
                    .font(.title2)
                    .fontWeight(.bold)
                    .foregroundColor(.darkBlue)
                    .cornerRadius(10)
                    .shadow(radius: 5)
                NavigationLink(destination: GestureHelpView() ,isActive: $isActiveHelp){
                    Button(action: {isActiveHelp=true})
                    {
                        Image(systemName: "info.circle")
                            .foregroundColor(.gray)
                        
                    }
                } }
            GeometryReader { geometry in
                VStack(alignment: .center,spacing: 24) {
        
                    
                    
                    
                    ZStack{
                        ForEach(0..<modelDataObservable.exerciseSets.count){ ind in
                            if(modelDataObservable.exerciseSets[ind].isFavorite == true && modelDataObservable.exerciseSets[ind].name != "Rest" ){
                                
                                CardView(name:modelDataObservable.exerciseSets[ind].name,image:modelDataObservable.exerciseSets[ind].image,exerciseModelLocal: modelDataObservable.exerciseSets[ind])
                                    
                                    .frame(width: self.getCardWidth(geometry, id: ind), height: 450)
                                    .offset(x: 0, y: self.getCardOffset(geometry, id: ind))
                                    .gesture(TapGesture(count: 2).onEnded { _ in
                                      
                                        tempRoutineInd.append(ind)
                                        tempRoutineName.append(modelDataObservable.exerciseSets[ind].name)
                                        tempRoutineStartAmount.append(startAmount)
                                        tempRoutineIncrease.append(increaseDaily)
                                        tempRoutineDuration.append(duration)
                                        self.modelDataObservable.exerciseSets[ind].isFavorite = false
                                        self.startAmount = 0
                                        increaseDaily = 0
                                        
                                    })
                                    .simultaneousGesture(DragGesture().onEnded {_ in
                                        self.modelDataObservable.exerciseSets[ind].isFavorite = false
                                        
                                    })
                            }
                            if (modelDataObservable.exerciseSets.allSatisfy({ $0.isFavorite == false })){
                                Text("You Checked all data")
                                
                            }
                            
                        }
                    }
                    
                    VStack{
                        
                        VStack{
                            
                            HStack{
                                Text("Start Amount: \(startAmount) ").bold().foregroundColor(.darkBlue)
                                Spacer()
                           
                                Stepper("", value: $startAmount,in:5...35,step: 5)}.foregroundColor(.darkBlue)
                            
                            HStack{
                                
                                
                                Text(increaseDaily==0 ? "Daily Increase: \(increaseDaily)":"Daily Increase: +\(increaseDaily)").bold().foregroundColor(.darkBlue)
                                Spacer()
                                Stepper("", value: $increaseDaily, in:0...15)}.foregroundColor(.darkBlue)
                            
                            
                            Text("Duration for Routine: " + String(format: "%.0f",duration)+" day").bold().foregroundColor(.darkBlue)
                            Slider(value: $duration, in: 1...30, step: 1.0).colorMultiply(Color.darkBlue)
                            
                        }
                        
                        
                        
                        
                        
                        NavigationLink(destination: ReminderView(selectedExercise: selectedExercise) ,isActive: $isActive) {
                            
                            
                            Button(action : {
                                isActive = true
                                selectedExercise.updateGlobalRoutineList["routineName"] = tempRoutineName
                                selectedExercise.updateGlobalRoutineList["startAmount"] = tempRoutineStartAmount
                                selectedExercise.updateGlobalRoutineList["increase"] = tempRoutineIncrease
                                selectedExercise.updateGlobalRoutineList["duration"] = tempRoutineDuration
                                selectedExercise.updateGlobalRoutineList["id"] = tempRoutineInd
                            })
                            {
                                Text("Next")
                                    .font(.system(size: 25 ,weight:.medium)).foregroundColor(.darkBlue).padding()
                                    .background(Color.white)
                                    .cornerRadius(15)
                                    .shadow( radius: 10)
                            }.padding(.bottom,20)
                            
                            
                        }
                        
                        
                    }.padding(.top,25)
                    
                }
            }
        }.padding(10)
    }
}





