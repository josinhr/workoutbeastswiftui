//
//  TimerView.swift
//  Workoutjose
//
//  Created by MacBook on 13.01.21.
//

import SwiftUI

struct TimerView: View {
    
    @ObservedObject var stopWatchManager = StopWatchManager()
    @EnvironmentObject var exerciseSetModel: ExerciseSetModel

    
    var body: some View {
        VStack{
                if stopWatchManager.mode == .stopped {
                    Button(action: {self.stopWatchManager.start()}){
                        TimerButton(label: "Start", buttonColor: .blue)
                    }.transition(.opacity)
                }
                
                if stopWatchManager.mode == .running {
                    Button(action: {self.stopWatchManager.pause()}){
                        TimerButton(label: "Pause", buttonColor: .blue)
                    }.transition(.opacity)
                }
                
                if stopWatchManager.mode == .paused {
                    Button(action: {self.stopWatchManager.start()}){
                        TimerButton(label: "Start", buttonColor: .blue)
                    }.transition(.opacity)
                    Button(action: {self.stopWatchManager.stop()}){
                        TimerButton(label: "Stop", buttonColor: .red)
                    }
                    .transition(.opacity)
                }
            }
        }
    }


struct TimerButton: View {
    let label: String
    let buttonColor: Color
    
    var body: some View{
        Text(label)
            .foregroundColor(.white)
            .padding(.vertical, 20)
            .padding(.horizontal, 90)
            .background(buttonColor)
            .cornerRadius(10)
        
    }
    
}




