//
//  TopHomeView.swift
//  Workoutjose
//
//  Created by MacBook on 29.01.21.
//

import SwiftUI

struct TopHomeView: View {
    @EnvironmentObject var quickActionSettings: QuickActionSettings
    @StateObject  var user = EditProfileMV()
    
    
    var body: some View {
        
        
        HStack(alignment: .center){
            Text(user.name)
                .fontWeight(.bold)
                .font(.largeTitle)
            
            
            
            Spacer()
            NavigationLink(
                destination: EditProfileDataView(),tag: .details(name: user.imageName), selection: $quickActionSettings.quickAction
            ){
                
                
                
                FloatingMenu(image: user.imageName,height: 35,width: 35, system: false)
            }
        }
    }
}

struct TopHomeView_Previews: PreviewProvider {
    static var previews: some View {
        TopHomeView()
    }
}
