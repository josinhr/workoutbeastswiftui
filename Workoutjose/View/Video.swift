//
//  Video.swift
//  Workoutjose
//
//  Created by MacBook on 20.01.21.
//

struct Video : Identifiable {
    
    var id : Int
    var player : AVPlayer
    var replay : Bool
}
