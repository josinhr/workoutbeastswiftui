//
//  WorkoutjoseApp.swift
//  Workoutjose
//
//  Created by MacBook on 30.12.20.
//

import SwiftUI
import UIKit

let quickActionSettings = QuickActionSettings()
var shortcutItemToProcess: UIApplicationShortcutItem?

@main
struct WorkoutjoseApp: App {
    @Environment(\.scenePhase) var phase
    @UIApplicationDelegateAdaptor(AppDelegate.self) var appDelegate
    
    var body: some Scene {
        WindowGroup {
            ContentView().environmentObject(ExerciseSetModel()) .environmentObject(quickActionSettings)
                .environmentObject(RoutineSetModel())
        } .onChange(of: phase) { (newPhase) in
            switch newPhase {
            case .active :
                print("App in active")
                guard let name = shortcutItemToProcess?.userInfo?["name"] as? String else {
                    return
                }
                switch name {
                case "stats":
                    
                    quickActionSettings.quickAction = .details(name: name)
              
                case "profile":
                    
                    quickActionSettings.quickAction = .details(name: name)
                default:
                    print("default ")
                }
            case .inactive:
                print("App is inactive")
            case .background:
                
                addQuickActions()
            @unknown default:
                print("default")
            }
        }
    }
    
    func addQuickActions() {
        var calluserInfo: [String: NSSecureCoding] {
            return ["name" : "stats" as NSSecureCoding]
        }
      
        var contactuserInfo: [String: NSSecureCoding] {
            return ["name" : "profile" as NSSecureCoding]
        }
        
        UIApplication.shared.shortcutItems = [
            UIApplicationShortcutItem(type: "Stadistics", localizedTitle: "Stadistics", localizedSubtitle: "", icon: UIApplicationShortcutIcon(type: .favorite), userInfo: calluserInfo),
           
            UIApplicationShortcutItem(type: "Profile", localizedTitle: "Profile", localizedSubtitle: "", icon: UIApplicationShortcutIcon(type: .contact), userInfo: contactuserInfo),
        ]
    }
}

class AppDelegate: NSObject, UIApplicationDelegate {
    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        if let shortcutItem = options.shortcutItem {
            shortcutItemToProcess = shortcutItem
        }
        
        let sceneConfiguration = UISceneConfiguration(name: "Custom Configuration", sessionRole: connectingSceneSession.role)
        sceneConfiguration.delegateClass = CustomSceneDelegate.self
        
        return sceneConfiguration
    }
}

class CustomSceneDelegate: UIResponder, UIWindowSceneDelegate {
    func windowScene(_ windowScene: UIWindowScene, performActionFor shortcutItem: UIApplicationShortcutItem, completionHandler: @escaping (Bool) -> Void) {
        shortcutItemToProcess = shortcutItem
    }
}


