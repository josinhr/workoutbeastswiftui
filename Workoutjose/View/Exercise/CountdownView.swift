//
//  CountdownView.swift
//  Workoutjose
//
//  Created by MacBook on 26.01.21.
//

import SwiftUI
let timer = Timer
    .publish(every: 1, on: .main, in: .common)
    .autoconnect()

struct Clock: View {
    var counter: Int
    var countTo: Int
    var body: some View {
        VStack {
            Text(counterToMinutes())
                .font(.title)
                .fontWeight(.black)
                .foregroundColor(.black)
        }.frame(width: UIScreen.main.bounds.width/2.5, height: UIScreen.main.bounds.height/4.5)
    }
    func counterToMinutes() -> String {
        let currentTime = countTo - counter
        let seconds = currentTime % 60
        let minutes = Int(currentTime / 60)
        return "\(minutes):\(seconds < 10 ? "0" : "")\(seconds)"
    }
}
struct ProgressTrack: View {
    var body: some View {
        Circle()
            .fill(Color.clear)
            .frame(width: UIScreen.main.bounds.width/2.5, height: UIScreen.main.bounds.height/4.5)
            .overlay(
                Circle().stroke(Color.white, lineWidth: 15)
            )
    }
}
struct ProgressBar: View {
    var counter: Int
    var countTo: Int
    var body: some View {
        Circle()
            .fill(Color.clear)
            .frame(width: UIScreen.main.bounds.width/2.5, height: UIScreen.main.bounds.height/4.5)
            .overlay(
                Circle().trim(from:0, to: progress())
                    .stroke(
                        style: StrokeStyle(
                            lineWidth: 15,
                            lineCap: .round,
                            lineJoin:.round
                        )
                    )
                    .foregroundColor(
                        (completed() ? Color.green : Color.darkBlue)
                    ).animation(
                        .easeInOut(duration: 0.2)
                    )
            )
    }
    func completed() -> Bool {
        return progress() == 1
    }
    func progress() -> CGFloat {
        return (CGFloat(counter) / CGFloat(countTo))
    }
}
struct CountdownView: View {
    @State var counter: Int = 0
    var countTo: Int
    var body: some View {
        VStack{
            ZStack{
                ProgressTrack().shadow(radius: 10)
                ProgressBar(counter: counter, countTo: countTo).shadow(radius: 10)
                Clock(counter: counter, countTo: countTo).shadow(radius: 10)
            }
        }.onReceive(timer) { time in
            if (self.counter < self.countTo) {
                self.counter += 1
            }
        }
    }
}
struct CountdownView_Previews: PreviewProvider {
    static var previews: some View {
        CountdownView(countTo: 120)
    }
}
