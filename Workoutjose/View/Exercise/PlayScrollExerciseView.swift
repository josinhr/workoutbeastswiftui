//
//  RandomExerciseView.swift
//  Workoutjose
//
//  Created by MacBook on 20.01.21.
//

import SwiftUI
import AVKit

struct RandomExerciseView: View {
    var routine: RoutineModel

    
    var body: some View{
        
        GeometryReader { geometry in
            VStack {
                
                
                PlayerView(data:routine, contentSize: geometry.size.height )
            }
            .frame(minWidth: 0, maxWidth: .infinity, minHeight: 0, maxHeight: .infinity, alignment: Alignment.topLeading)
        }
        
        
    }
    
}

struct PlayerView : View {
    
    var data : RoutineModel
    var contentSize: CGFloat
    @State var showingControls = false
  
    var body: some View{
        
        VStack(spacing: 0){
            
            List{
            ForEach(0..<self.data.exerciseTime.count){i in
              
                ZStack{
                    VStack{
                        WebVideoView(videoLink: self.data.exerciseTime[i].exercise.link)
                            .frame(width: UIScreen.main.bounds.width, height: contentSize*2/3)
                            .offset(y: -5)
                        
                        CountdownView(countTo: self.data.exerciseTime[i].time)
                            .frame(width: UIScreen.main.bounds.width, height: contentSize/3)
                            .offset(y: -5)
                            
                            
                        
                    }
                }
            }
        }
        }
    }
}


struct PlayScrollExerciseView_Previews: PreviewProvider {
    static let modelData = ExerciseSetModel()
    static let exerciseTime = [
        ExerciseTime(id: 1, time: 30, exercise: modelData.exerciseSets[0]),
        ExerciseTime(id: 2, time: 30, exercise: modelData.exerciseSets[1]),
        ExerciseTime(id: 3, time: 30, exercise: modelData.exerciseSets[2]),
        ExerciseTime(id: 4, time: 30, exercise: modelData.exerciseSets[3])]
    
    
    
    static var previews: some View {
        let routine = RoutineModel(id: 1, date: Date(), exerciseTime:exerciseTime)
        
     RandomExerciseView(routine: routine)
            .environmentObject(ExerciseSetModel())
    }

}
