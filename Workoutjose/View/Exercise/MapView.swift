//
//  MapView.swift
//  Workoutjose
//
//  Created by Zohrali Abbasov on 06.02.21.
//

import SwiftUI
import MapKit


struct MapView: UIViewRepresentable {
    
    @Binding var locationManager: CLLocationManager
    @Binding var showMapAlert: Bool
    @Binding var distance: String
    static var resetDistance = false
    let map = MKMapView()
    
    let mapUIView = MapUIView()
    
    
    
    func makeUIView(context: Context) -> MKMapView {
        locationManager.delegate = context.coordinator
        map.delegate = context.coordinator
        locationManager.requestWhenInUseAuthorization()
        return map
    }
    
    
    func updateUIView(_ uiView: MKMapView, context: Context) {
        map.showsUserLocation = true
        map.userTrackingMode = .follow
    }
    
    
    
    func makeCoordinator() -> MapView.Coordinator {
        return Coordinator(mapView: self)
    }
    
    class Coordinator: NSObject, CLLocationManagerDelegate, MKMapViewDelegate {
        
        var mapView: MapView
        var locationArray = [CLLocationCoordinate2D]()
        var polyLine = MKPolyline()
        
        var isZoomed = false
        init(mapView: MapView) {
            self.mapView = mapView
        }
        
        func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
            switch status {
            case .restricted:
                break
            case .denied:
                mapView.showMapAlert.toggle()
                return
            case .notDetermined:
                mapView.locationManager.requestWhenInUseAuthorization()
                return
            case .authorizedWhenInUse:
                mapView.locationManager.allowsBackgroundLocationUpdates = true
                mapView.locationManager.pausesLocationUpdatesAutomatically = false
                return
            case .authorizedAlways:
                mapView.locationManager.allowsBackgroundLocationUpdates = true
                mapView.locationManager.pausesLocationUpdatesAutomatically = false
                return
            @unknown default:
                break
            }
            mapView.locationManager.startMonitoringSignificantLocationChanges()
        }
        
        
        func mapView(_ mapView: MKMapView, didUpdate userLocation: MKUserLocation) {
            let loc = CLLocationCoordinate2D(latitude: userLocation.coordinate.latitude, longitude: userLocation.coordinate.longitude)
   
            self.locationArray.append(loc)
            
            
            let firstLoc = self.locationArray.first
            let lastLoc = self.locationArray.last
            
            if resetDistance == true {
                self.locationArray.removeAll()
            }
            
            let start = CLLocation(latitude: firstLoc!.latitude, longitude: firstLoc!.longitude)
            
            let end = CLLocation(latitude: lastLoc!.latitude, longitude: lastLoc!.longitude)
            
            
            let distanceInMeters = end.distance(from: start)
            let distanceInKm = distanceInMeters / 1000
            let kmStr = "\(distanceInKm.rounded(toPlaces: 2))"
            self.mapView.distance = kmStr
            if !isZoomed {
                let region = MKCoordinateRegion(center: loc, span: MKCoordinateSpan(latitudeDelta: 0.01, longitudeDelta: 0.01))
                
                self.mapView.map.setRegion(region, animated: true)
                isZoomed = true
            }
            
            
            polyLine = MKPolyline(coordinates: self.locationArray, count: self.locationArray.count)
            
            if MapUIView.isActive == true {
                self.mapView.map.addOverlay(polyLine)
            }
            else{
                self.mapView.map.removeOverlays(mapView.overlays)
            }
            
            
        }
        
        func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
            if let polyLine = overlay as? MKPolyline {
                let polyLineRenderer = MKPolylineRenderer(overlay: polyLine)
                polyLineRenderer.strokeColor = UIColor.blue
                polyLineRenderer.lineWidth = 2.0
                
                return polyLineRenderer
            }
            
            return MKPolylineRenderer()
        }
    }
}


extension Double {
    func rounded(toPlaces places:Int) -> Double {
        let divisor = pow(10.0, Double(places))
        return (self * divisor).rounded() / divisor
    }
}

