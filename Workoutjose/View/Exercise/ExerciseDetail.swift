//
//  ExerciseDetail.swift
//  Workoutjose
//
//  Created by MacBook on 12.01.21.
//

import SwiftUI
import AVKit


struct ExerciseDetail: View {
    @EnvironmentObject var exerciseSetModel: ExerciseSetModel
    var exercise: ExerciseModel

    var exerciseIndex: Int {
        exerciseSetModel.exerciseSets.firstIndex(where: { $0.id == exercise.id })!
    }

    var body: some View {
        ScrollView {
            VStack{
                WebVideoUnresizedView(player: PlayerUIView(frame: .zero, videoLink: exercise.link)).frame(height: UIScreen.main.bounds.height / 2.3).cornerRadius(25)
                    .padding()
                    .shadow(radius: 15)
                Spacer()
            VStack(alignment: .leading) {
                HStack {
                    Text(exercise.name)
                        .font(.title)
                        .fontWeight(.bold)
                        .foregroundColor(Color( red: 0.42, green: 0.36, blue: 0.91, opacity: 1.0))
                }
                Divider()
                VStack {

                Text("\(exercise.description)")
                    .font(.title2).fontWeight(.medium).padding().foregroundColor(.white)
                    
                    
                } .padding(.horizontal)

                .background(Color.darkBlue)
                .cornerRadius(25)
     
                .shadow(radius: 15)
                
                Spacer(minLength: 28)
                
                Divider()
               
                if (exercise.name != "Rest"){
                Text("Muscles involved")
                    .font(.title2)
                    .fontWeight(.medium)
                    .foregroundColor(Color( red: 0.42, green: 0.36, blue: 0.91, opacity: 1.0))
                
                Spacer(minLength: 28)
                
                
                Image(getPicNameForTargetMuscle(exerciseName: exercise.name))
                    .resizable()
                    .padding(.horizontal)
                    .frame(width: 360, height: 400, alignment: .center)
                    .shadow(radius: 15)
                }

            }
            .padding()
        }
      
    }
        
        
}

struct player: UIViewControllerRepresentable{
    
    var videoLink: String
    
    func makeUIViewController(context: UIViewControllerRepresentableContext<player>) -> AVPlayerViewController {
        let player1 = AVPlayer(url: URL(string: videoLink)!)
        let controller = AVPlayerViewController()
        controller.player = player1
        return controller
    }
    
    func updateUIViewController(_ uiViewController: AVPlayerViewController, context: UIViewControllerRepresentableContext<player>) {
        
    }
}

struct LandmarkDetail_Previews: PreviewProvider {
    static let modelData = ExerciseSetModel()

    static var previews: some View {
        ExerciseDetail(exercise: modelData.exerciseSets[0])
            .environmentObject(modelData)
    }
}
    
    func getPicNameForTargetMuscle (exerciseName: String) -> String {
        switch exerciseName {
        case "Push-Ups": return "pushupsEx"
        case "Run":      return "runningEx"
        case "Squats":   return "squatsEx"
        case "Biceps":   return "bicepsEx"
        case "Pull-Ups": return "pullupsEx"
        case "Bench Press": return "benchPressEx"
        case "ABS Workout": return "absEx"
        case "Boxing": return "boxingEx"
        case "Skipping": return "skippingEx"
        case "Swimming": return "swimmingEx"
        case "Cross Trainer": return "crossTrainerEx"
        case "Weight Lifting": return "weightLiftingEx"
        case "Deadlifting": return "deadliftingEx"
        
        default:
            return "relax"
        }
    }
}

