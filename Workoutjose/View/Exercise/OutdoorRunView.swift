//
//  TopMainView.swift
//  Workoutjose
//
//  Created by MacBook on 02.01.21.
//

import SwiftUI

struct OutdoorRunView: View {
    
    var body: some View {
        
        NavigationLink(destination: MapUIView()) {
            HStack() {
                Spacer()
                Image("run") .resizable()
                    .frame(width: 80, height: 80)
                    .scaledToFit()
                
                
                Text("Outdoor Cardio")
                    .font(.system(size: 24,weight:.semibold))
                    .foregroundColor(.white)
                Spacer()
            }        .background(Color.darkBlue)
            .cornerRadius(9)
            .shadow(radius: 10)
            
        }.shadow(radius: 10)
        
    }
}


struct OutdoorRunView_Previews: PreviewProvider {
    static var previews: some View {
        OutdoorRunView()
    }
}

