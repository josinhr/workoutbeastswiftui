//
//  ReminderView.swift
//  Workoutjose
//
//  Created by Alisafa Gasimov  on 1/24/21.
//

import SwiftUI

struct ReminderView: View {
    @State private var wakeUp = Date()
    @State private var startDate = Date()
    @State  var notificatioState = true
    @ObservedObject var selectedExercise : RoutineRetrieveModel
    
    static let routineModelData = RoutineSetModel()
    
    
    @State var isActiveHome :Bool = false
    
    func scheduleNotification() -> Void {
        let content = UNMutableNotificationContent()
        content.title = "Alarm"
        content.body = "body"
        
        var reminderDate = wakeUp
        
        if reminderDate < Date() {
            if let addedValue = Calendar.current.date(byAdding: .day, value: 1, to: reminderDate) {
                reminderDate = addedValue
            }
        }
        
        let comps = Calendar.current.dateComponents([.year, .month, .day, .hour, .minute], from: reminderDate)
        
        let trigger = UNCalendarNotificationTrigger(dateMatching: comps, repeats: false)
        
        let request = UNNotificationRequest(identifier: "alertNotificationUnique", content: content, trigger: trigger)
        
        UNUserNotificationCenter.current().add(request) {(error) in
            if let error = error {
                print("\(error)")
            }
        }
    }
    
    
    
    func requestPush() -> Void {
        UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .badge, .sound]) { success, error in
            if success {
                print("Success")
            } else if let error = error {
                print(error.localizedDescription)
            }
        }
    }
    
    var body: some View {
        
        ScrollView {
            VStack(){
                
                
                Text("Choose a start date ").foregroundColor(.darkBlue).fontWeight(.semibold).font(.title)
                HStack{
                DatePicker("", selection:$startDate , displayedComponents: .date)
                    .datePickerStyle(WheelDatePickerStyle()).colorMultiply(Color.darkBlue)
                Spacer()
                }
                Toggle(isOn:  self.$notificatioState, label: {
                    Image(systemName: "alarm")
                    Text("Nofify me")
                    
                }).padding()
                
                if self.notificatioState{
                    DatePicker("", selection: ($wakeUp), displayedComponents: .hourAndMinute)
                        .datePickerStyle(WheelDatePickerStyle()).colorMultiply(Color.darkBlue)
                        .frame(height :150).padding()
                    
                }
                
                Spacer()
                
                
                HStack{
                    Button(action: {
                        selectedExercise.updateGlobalRoutineList.removeAll()
                        isActiveHome = true
                    })
                    {
                        
                        Text("Cancel")
                            .font(.system(size: 25 ,weight:.medium)).foregroundColor(.darkBlue).padding()
                            .background(Color.white)
                            .cornerRadius(15)
                            .shadow( radius: 10)
                        
                    }.padding()
                    
                    
                    Button(action: {
                        
                        
                        selectedExercise.updateGlobalRoutineList["startDate"] = "\($startDate.wrappedValue)";
                        selectedExercise.writeToJson()
                        selectedExercise.updateGlobalRoutineList.removeAll()
                        isActiveHome = true
                        if self.notificatioState  {
                            requestPush();
                            scheduleNotification()};
                    } )
                    {
                        Text("Create")
                            .font(.system(size: 25 ,weight:.medium)).foregroundColor(.darkBlue).padding()
                            .background(Color.white)
                            .cornerRadius(15)
                            .shadow( radius: 10)
                        
                    }.padding()
                }
                
            }.padding()
            NavigationLink(destination: MainHomeView(), isActive: $isActiveHome) { }
            
            
        } .navigationTitle("Reminder")
        
    }
}


