//
//  MapUIView.swift
//  Workoutjose
//
//  Created by Zohrali Abbasov on 06.02.21.
//

import SwiftUI
import MapKit

struct MapUIView: View {
    @State var hours: Int = 0
    @State var minutes: Int = 0
    @State var seconds: Int = 0
    @State var timerIsPaused: Bool = true
    @State var timer: Timer? = nil
    @State var buttonName = "Start"
    @State var buttonColor = Color.darkBlue
    
    @State var locationManager = CLLocationManager()
    @State var showMapAlert = false
    @State var distance = "0.0"
    
    @State var calories: Double = 0.0
    
    static var isActive : Bool = false
    
    @StateObject var user = EditProfileMV()
    
    
    
    func goToDeviceSettings() {
        guard let url = URL.init(string: UIApplication.openSettingsURLString) else { return }
        UIApplication.shared.open(url, options: [:], completionHandler: nil)
        
    }
    

        func calculateCalories(gender: String, age: Int, weight: Double, heartRate: Int, h:Int, m:Int, s:Int ) -> Double {
            var calories : Double
            let totalMinutes = Double (h) * 60 + Double(m)  + Double(s)/60
            if gender == "Male" {
                let ageIndex = Double(age) * 0.2017
                let weightIndex = weight *  0.09036
                let heartRateIndex = Double (heartRate) * 0.6309
                let timeIndex = totalMinutes / 4.184
                calories = (ageIndex - weightIndex + heartRateIndex - 35.0969) * timeIndex
            }
            else{
                let ageIndex = Double(age) * 0.074
                let weightIndex = weight *  0.05741
                let heartRateIndex = Double (heartRate) * 0.4472
                let timeIndex = totalMinutes / 4.184
                calories = (ageIndex - weightIndex + heartRateIndex - 20.4022) * timeIndex
    
            }
            return calories
        }
    
    
    func displayTimer(h: Int, m: Int, s: Int) -> String {
        var second = "\(s)"
        var minute = "\(m)"
        var hour = "\(h)"
        
        if s < 10 {
            second = "0" + second
        }
        
        if m < 10 {
            minute = "0" + minute
        }
        
        if minutes < 10 {
            hour = "0" + hour
        }
        
        return "\(hour):\(minute):\(second)"
    }
    
    func startTimer(){
        timerIsPaused = false
        timer = Timer.scheduledTimer(withTimeInterval: 1, repeats: true){ tempTimer in
            
            if self.seconds == 59 {
                self.seconds = 0
                if self.minutes == 59 {
                    self.minutes = 0
                    self.hours = self.hours + 1
                } else {
                    self.minutes = self.minutes + 1
                }
            } else {
                self.seconds = self.seconds + 1
            }
        
            
        }
    }
    
    func startPause (){
        
        
        if  buttonName == "Start" || buttonName ==  "Continue" {
            buttonName = "Pause"
            buttonColor = Color.yellow
            startTimer()
            MapUIView.isActive = true
            MapView.resetDistance = false
           
        }
        else{
            buttonName = "Continue"
            buttonColor = Color.darkBlue
            pauseTimer()
            MapView.resetDistance = false
        }
        
    }
    
    func pauseTimer(){
        timerIsPaused = true
        timer?.invalidate()
        timer = nil
    }
    
    func stopTimer(){
        buttonName = "Start"
        buttonColor = Color.darkBlue
        pauseTimer()
        hours = 0
        minutes = 0
        seconds = 0
        calories = 0.0
        MapUIView.isActive = false
        MapView.resetDistance = true
    }
    var body: some View {
        
        VStack {
            ZStack {
                MapView(locationManager: $locationManager, showMapAlert: $showMapAlert, distance: $distance)
                    .alert(isPresented: $showMapAlert) {
                        Alert(title: Text("Location access denied"),
                              message: Text("Your location is needed"),
                              primaryButton: .cancel(),
                              secondaryButton: .default(Text("Settings"),
                                                        action: { self.goToDeviceSettings() }))
                    }
                    .navigationTitle("Map")
                
                
                
            }
            Spacer()
            VStack {
                HStack {
                    VStack{
                        
                        
                        Text(verbatim: distance)
                            .font(.title)
                            .fontWeight(.medium)
                        Text("Distance (km)")
                            .italic()
                    }
                    
                    
                    VStack{
                        
                        
                        Text(String(format: "%.1f", (calculateCalories(gender: user.gender, age: user.age, weight: user.weight, heartRate: 75, h: hours, m: minutes, s: seconds))))
                            .font(.title)
                            .fontWeight(.medium)
                        
                        Text("Calories (cal)")
                            .italic()
                    }
                    
                    
                    VStack{
                        Text(displayTimer(h: hours, m: minutes, s: seconds))
                            .font(.title)
                            .fontWeight(.medium)
                        
                        Text("Duration")
                            .italic()
                    }
                    
                    
                }
                .padding(.bottom)
                HStack {
                    
                    ControlButton(text: buttonName, color: buttonColor, calledFunction: startPause)
                    
                    ControlButton(text: "Stop", color: Color.red, calledFunction: stopTimer)
                    
                }
            }
        }
    }
    
}




struct MapUIView_Previews: PreviewProvider {
    static var previews: some View {
        MapUIView()
    }
}

struct ControlButton: View {
    var text : String
    let color : Color
    let calledFunction : () -> Void
    var body: some View {
        Button(action: calledFunction) {
            Text(text)
                .fontWeight(.bold)
                .font(.title)
                .padding()
                .background(color)
                .cornerRadius(40)
                .foregroundColor(.white)
                .padding(10)


        }
        
        .shadow(radius: 10)
    }
}


