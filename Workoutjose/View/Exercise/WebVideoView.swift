//
//  WebVideoView.swift
//  Workoutjose
//
//  Created by MacBook on 13.01.21.
//

import SwiftUI
import AVFoundation
import AVKit



struct WebVideoView: View {
    
    @ObservedObject var player:PlayerUIView
    
    
    var body: some View {
        ZStack{
            WebVideoUnresizedView(player: player)
                .frame(width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height/2.8)
            
        }
    }
}

struct WebVideoUnresizedView: UIViewRepresentable {
    
    
    
    @ObservedObject var player :PlayerUIView
    
    func makeUIView(context: Context) -> UIView {
        return player
        
    }
    
    func updateUIView(_ uiView: UIView, context: Context) {
        
    }
}


class PlayerUIView: UIView,ObservableObject {
    private var playerLooper: AVPlayerLooper?
    private var playerLayer = AVPlayerLayer()
    @Published var player : AVQueuePlayer
    var videoLink: String
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    init(frame: CGRect, videoLink:String) {
        self.videoLink = videoLink
        self.player = AVQueuePlayer()
        super.init(frame: frame)
        let asset = AVAsset(url: URL(string: videoLink)!)
        let item = AVPlayerItem(asset: asset)
        playerLayer.player = player
        playerLayer.videoGravity = .resizeAspectFill
        layer.addSublayer(playerLayer)
        playerLooper = AVPlayerLooper(player: player, templateItem: item)
        player.play()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        playerLayer.frame = bounds
    }
}


struct WebVideoView_Previews: PreviewProvider {
    static var previews: some View {
        WebVideoView(player: PlayerUIView(frame: .zero, videoLink: "https://cdn.videvo.net/videvo_files/video/free/2018-05/small_watermarked/180419_Boxing_13_05_preview.mp4"))
    }
}
