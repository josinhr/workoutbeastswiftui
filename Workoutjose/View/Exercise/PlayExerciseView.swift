//
//  PlayExerciseView.swift
//  Workoutjose
//
//  Created by MacBook on 13.01.21.
//

import SwiftUI

struct PlayExerciseView: View {
    @State var showingControls = false
    
    
   
    var exercise: ExerciseModel
    
    var body: some View {
        WebVideoView(videoLink: exercise.link)
        TimerView(showingControls: self.$showingControls)
            .gesture(
                TapGesture()
                    .onEnded({ (_) in
                        self.showingControls.toggle()
                    }) )
            
        Spacer()
    }
}





struct PlayExerciseView_Previews: PreviewProvider {
    @EnvironmentObject var exerciseSetModel: ExerciseSetModel
    static var previews: some View {
        let modelData = ExerciseSetModel()
        
        PlayExerciseView(exercise: modelData.exerciseSets[0])
            .environmentObject(modelData)
    }
}
