//
//  AfterLaunchView.swift
//  Workoutjose
//
//  Created by MacBook on 25.01.21.
//

import SwiftUI
import HealthKit


struct AfterLaunchView : View {
    
    @State var animate = false
    @State var endSplash = false
    
    var body: some View {
        
        ZStack{
            
            MainHomeView()
            
            ZStack{
                
                Color("bg")
                Image("workoutBeastLaunchLogo")
                    .resizable()
                    .renderingMode(.original)
                    .aspectRatio(contentMode: animate ? .fill : .fit)
                    .frame(width: animate ? nil : 85, height: animate ? nil : 85)
                    .scaleEffect(animate ? 3 : 1)
                    .frame(width: UIScreen.main.bounds.width)
            }
            .ignoresSafeArea(.all, edges: .all)
            .onAppear(perform: animateSplash)
            .opacity(endSplash ? 0 : 1)
        }
    }
    
    func animateSplash() {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.85) {
            withAnimation(Animation.easeOut(duration: 0.75)) {
                animate.toggle()
            }
            withAnimation(Animation.linear(duration: 0.45)) {
                endSplash.toggle()
            }
        }
    }
    
    
}

