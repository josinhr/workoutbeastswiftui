//
//  RoutineItemView.swift
//  Workoutjose
//
//  Created by MacBook on 29.01.21.
//

import SwiftUI

struct RoutineItemView: View {
    
    var routine: RoutineModel
    
    var body: some View {
        ZStack {
            if(routine.date > Date()){
                RoundedRectangle(cornerRadius: 25, style: .continuous)
                    .fill(Color.white)
                    .shadow(radius: 10)}
            else{
                RoundedRectangle(cornerRadius: 25, style: .continuous)
                    .fill(Color.gray.opacity(0.6))
            }
            
            VStack {
                Text(
                    "\(routine.date.get(.day, .month, .year).day!)")
                    .font(.system(size: 70))
                    .foregroundColor(.darkBlue)
                
                Text( routine.date.month)
                    .font(.headline)
                    .foregroundColor(.black)
                
            }
            .padding(20)
            .multilineTextAlignment(.center)
        }.frame(width: 200, height: 220)
    }
}

extension Date {
    func get(_ components: Calendar.Component..., calendar: Calendar = Calendar.current) -> DateComponents {
        return calendar.dateComponents(Set(components), from: self)
    }
    
    func get(_ component: Calendar.Component, calendar: Calendar = Calendar.current) -> Int {
        return calendar.component(component, from: self)
    }
    var month: String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MMMM"
        return dateFormatter.string(from: self)
    }
}

