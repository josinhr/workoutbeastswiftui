//
//  RoutineHomeView.swift
//  Workoutjose
//
//  Created by MacBook on 29.01.21.
//

import SwiftUI

struct RoutineHomeView: View {
    
    var routines : [RoutineModel]
    @EnvironmentObject var quickActionSettings: QuickActionSettings
    
    init(routines:[RoutineModel]) {
        self.routines = routines
    }
    var body: some View {
        VStack(alignment: .leading) {
            Text("Routines")
                .font(.title2)
                .fontWeight(.bold)
                .foregroundColor(.darkBlue)
            ScrollView(.horizontal, showsIndicators: false) {
                
                HStack(alignment: .top, spacing: 15) {
                    
                    ForEach(routines) { routine in
                         if(routine.date < Date()){
                            RoutineItemView(routine:routine)
                        }
                        else{
                            NavigationLink(destination: RoutinePreviewView(routine: routine)) {
                                RoutineItemView(routine:routine)
                            }
                        }
                    }
                    AddRoutineItemView()
                }.padding()
            }
            .frame(height: 230)
        }
    }
}



struct AddRoutineItemView: View {
    @EnvironmentObject var quickActionSettings: QuickActionSettings
    @State var isActiveRoutine : Bool = false
    
    var body: some View {
        NavigationLink(destination: CreateRoutineListView() , isActive:  $isActiveRoutine ) 
        {
            ZStack {
                RoundedRectangle(cornerRadius: 25, style: .continuous)
                    .fill(Color.white)
                    .shadow(radius: 10)
                
                VStack {
                    Image(systemName:"plus.circle")
                        .font(.system(size: 70,weight:.semibold))
                        .foregroundColor(.darkBlue)
                        .frame( height: 80)
                        .scaledToFit()
                    
                }
                .padding(20)
                .multilineTextAlignment(.center)
            }.frame(width: 200, height: 220)
        }
    }
}


struct RoutineHomeView_Previews: PreviewProvider {
    
    static let modelData = ExerciseSetModel()
    static let exerciseTime = [
        ExerciseTime(id: 1, time: 30, exercise: modelData.exerciseSets[0]),
        ExerciseTime(id: 2, time: 30, exercise: modelData.exerciseSets[1]),
        ExerciseTime(id: 3, time: 30, exercise: modelData.exerciseSets[2]),
        ExerciseTime(id: 4, time: 30, exercise: modelData.exerciseSets[3])]
    
    
    
    static var previews: some View {
        let routine = RoutineModel(id: 1, date: Date(), exerciseTime:exerciseTime)
        
        RoutineHomeView(routines: [routine])
            .environmentObject(ExerciseSetModel()).environmentObject(RoutineSetModel())
    }
}
