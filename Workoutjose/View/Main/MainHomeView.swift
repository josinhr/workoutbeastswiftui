//

//
//  HomeView.swift
//  Workoutjose
//
//  Created by MacBook on 29.01.21.
//

import SwiftUI

struct MainHomeView: View {
    
    @StateObject  var user = EditProfileMV()
    @EnvironmentObject var exerciseSetModel: ExerciseSetModel
    @EnvironmentObject var routineSetModel: RoutineSetModel
    @State var isAtMaxScale = false
    @EnvironmentObject var quickActionSettings: QuickActionSettings
    static let modelData = ExerciseSetModel()
    let routineModelData : RoutineSetModel

    private let animation = Animation.linear(duration: 1).repeatForever(autoreverses: true)
    private let maxScale: CGFloat = 4
    
    init() {
        routineModelData = RoutineSetModel()
    }
    var body: some View {
        NavigationView {
            ZStack() {
                
                ScrollView{
                    VStack(spacing:15) {
                        TopHomeView()
                        RectangleVideo()
                        
                        
                        OutdoorRunView()
                        
                        
                        ForEach(exerciseSetModel.categories.keys.sorted(), id: \.self) { key in
                            CategoryRowView(categoryName: key, items: exerciseSetModel.categories[key]!)
                        }
                        RoutineHomeView(routines: routineModelData.routineModels)
                    }
                    .navigationBarTitle("WorkoutBeast",displayMode: .inline)
                    .navigationBarTitle("")
                    .navigationBarHidden(true)
                    .navigationBarBackButtonHidden(true)
                    .padding()
                    Spacer()
                }
                VStack {
                    Spacer()
                    
                    NavigationLink(
                        destination: StatView(routines: routineModelData.routineModels),tag: .details(name: "stats"), selection: $quickActionSettings.quickAction
                    ){
                        FloatingMenu(image: "stats",height: 35,width: 35, system: false)
                    }
                }.padding()
            }
            
        }.navigationBarHidden(true)
    }
}

struct MainHomeView_Previews: PreviewProvider {
    static var previews: some View {
        MainHomeView().environmentObject(ExerciseSetModel())
    }
}
