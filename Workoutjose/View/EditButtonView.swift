//
//  ButtonView.swift
//  Workoutjose
//
//  Created by MacBook on 02.01.21.
//

import SwiftUI

struct EditButtonView: View {
    @StateObject  var user = EditProfileMV()
    
    var body: some View {
        HStack{
            Spacer()
        VStack(alignment: .leading){
        NavigationLink (
            destination: EditProfileDataView(user: user),
            label: {Text("Edit profile")
                .font(.title2)
                .foregroundColor(Color.black)
                .padding(.all)})
            
        }
            Spacer()
        }.border(Color.black)
}
}
