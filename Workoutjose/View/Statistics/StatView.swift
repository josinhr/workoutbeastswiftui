
import SwiftUI
import HealthKit


struct StatView : View {
    private var healthStore : HealthStore?
    var routines : [RoutineModel]
    @State private var data: [HealthKitDataModel] = [HealthKitDataModel]()
     var workoutData :[Daily] = []
    
    func getTodayWeekDay(date:Date)-> String{
           let dateFormatter = DateFormatter()
           dateFormatter.dateFormat = "EEEE"
           let weekDay = dateFormatter.string(from: date)
           return weekDay
     }
    
    func getWorkoutInMins(routine: RoutineModel) -> CGFloat{
        var mins: CGFloat = 0
        for ex in routine.exerciseTime{
            mins = mins + CGFloat(ex.time)
        }
        return mins*10
    }
    
    init(routines:[RoutineModel]){
        self.routines = routines
        healthStore = HealthStore()
        var data :[Daily] = []
        for routine in routines {
            if( routine.date <  Date()){
                data.append(Daily(id: routine.id, day: getTodayWeekDay(date: routine.date) , workoutInMins: getWorkoutInMins(routine: routine)))
            }
        }
        self.workoutData = data
       
    }
    
    
    private func updateUIFromStatistics(_ statisticsCollection: HKStatisticsCollection) {
        let startDate = Calendar.current.date(byAdding: .day, value: -7, to: Date())!
        let endDate = Date()
        var localArray: [HealthKitDataModel] = [HealthKitDataModel]()
        
        statisticsCollection.enumerateStatistics(from: startDate, to: endDate) { (statistics,
                                                                                  stop) in
            switch statistics.quantityType.identifier{
            case HKQuantityTypeIdentifier.stepCount.rawValue:
                localArray.append(HealthKitDataModel(title: "Steps", currentData: Int(statistics.sumQuantity()?.doubleValue(for: .count()) ?? 0), date: statistics.startDate, color: Color.blue))
                break
            case HKQuantityTypeIdentifier.activeEnergyBurned.rawValue:
                localArray.append(HealthKitDataModel(title: "Energy",currentData: Int(statistics.sumQuantity()?.doubleValue(for: .kilocalorie()) ?? 0), date: statistics.startDate, color: Color.red))
                break
            case HKQuantityTypeIdentifier.dietaryWater.rawValue:
                localArray.append(HealthKitDataModel(title: "Water",currentData: Int(statistics.sumQuantity()?.doubleValue(for: .liter()) ?? 0), date: statistics.startDate, color: Color.darkBlue))
                break
                
            default:
                break
            }
            
        }
        data.append(contentsOf: localArray)
    }
    
    
    @State var selected = 0
    var colors = [Color.darkBlue, Color.black]
    var columns = Array(repeating: GridItem(.flexible(), spacing: 20), count:2)
    @StateObject  var user = EditProfileMV()
    
    
    
    
    var body: some View {
        ScrollView(.vertical, showsIndicators: false){
            VStack{
                if(workoutData.count > 0){
                HStack{
                    Text ("Hello, " + user.name)
                        .font(.title)
                        .fontWeight(.bold)
                    
                    
                    Spacer (minLength: 0)
                }
                .padding()
              
                VStack(alignment: .leading, spacing: 25) {
                    Text("Last days workout in minutes")
                        .font(.system(size: 22))
                        .fontWeight(.bold)
                        .foregroundColor(.darkBlue)
                    
                    HStack(spacing: 15){
                        ForEach(workoutData){ work in
                            VStack{
                                
                                VStack{
                                    Spacer(minLength: 0)
                                    
                                    if selected == work.id {
                                        Text(getHours(value: work.workoutInMins))
                                            .foregroundColor(Color.black)
                                            .padding(.bottom, 5)
                                            .minimumScaleFactor(0.04)
                                            .lineLimit(1)
                                    }
                                    
                                    Rectangle()
                                        .fill(LinearGradient(gradient: .init(colors: selected == work.id ? colors : [Color.black.opacity(0.20)]), startPoint: .top, endPoint: .bottom))
                                        .frame(height: getHeight(value: work.workoutInMins))
                                        .cornerRadius(7)
                                }
                                .frame(height: 220)
                                .onTapGesture {
                                    withAnimation(.easeOut){
                                        
                                        selected = work.id
                                    }
                                }
                                
                                Text(work.day)
                                    .font(.caption)
                                    .foregroundColor(.black)
                                    .lineLimit(1)
                                    .minimumScaleFactor(0.04)
                                
                                
                            }
                            
                        }
                    }
                    
                }
                .padding()
                .background(Color.white)
                .cornerRadius(25)
                .padding()
                .shadow(radius: 10)}
                
                HStack{
                    Text ("Last Statistics")
                        .font(.title)
                        .fontWeight(.bold)
                    
                    
                    Spacer (minLength: 0)
                }
                .padding()
                
                LazyVGrid(columns: columns , spacing: 30) {
                    ForEach(data){ stat in
                        SingleStatView(stat: stat)
                    }
                }.padding()
                .onAppear(){
                    if let healthStore = healthStore{
                        healthStore.requestAuthorization{ success in
                            if success {
                                healthStore.calculateStatistic { statisticsCollection in
                                    if let statisticsCollection = statisticsCollection{
                                        updateUIFromStatistics(statisticsCollection)
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    
 
    func getHeight(value: CGFloat) -> CGFloat {
        let hrs = CGFloat(value / 1440) * 200
        return hrs
    }
    

    func getHours(value: CGFloat) -> String {
        let hrs = value / 60
        return String(format: "%.1f", hrs)
    }
    
}

struct Daily : Identifiable {
    var id : Int
    var day : String
    var workoutInMins : CGFloat
}
