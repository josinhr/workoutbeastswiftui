//
//  SingleStatView.swift
//  Workoutjose
//
//  Created by MacBook on 30.01.21.
//

import SwiftUI

struct SingleStatView: View {
    var stat:HealthKitDataModel
    
    var body: some View {
        
        VStack(spacing: 32){
            
            HStack{
                
                Text(stat.title + " " + getType(val: stat.title))
                    .font(.system(size: 22))
                    .fontWeight(.bold)
                    .foregroundColor(.darkBlue)
                    .lineLimit(2)
                    .minimumScaleFactor(0.4)
           
                
                Text(getDate(val: stat.date))
                    .font(.body)
                    .foregroundColor(.gray)
                    .lineLimit(1)
                    .minimumScaleFactor(0.5)
                
            }
            ZStack {
                
                Circle()
                    .trim(from: 0, to: 1)
                    .stroke(stat.color.opacity(0.05), lineWidth: 10)
                    .frame(width: (UIScreen.main.bounds.width - 150)/2, height: (UIScreen.main.bounds.width-150)/2)
                
                
                Circle()
                    .trim(from: 0, to: CGFloat((stat.currentData )))
                    .stroke(stat.color, style: StrokeStyle(lineWidth: 10, lineCap: .round))
                    .frame(width: (UIScreen.main.bounds.width - 150)/2, height: (UIScreen.main.bounds.width-150)/2)
                
                Text(String(stat.currentData))
                    .font(.system(size: 22))
                    .fontWeight(.bold)
                    .foregroundColor(stat.color)
                    .rotationEffect(.init(degrees: 90))
            }
            .rotationEffect(.init(degrees: -90))
            
            
        }
        .padding()
        .background(Color.white)
        .cornerRadius(15)
        .shadow( radius: 10)
        
        
        
    }
    
    func getDate (val: Date) -> String {
        let formatter1 = DateFormatter()
        formatter1.dateStyle = .short
        return formatter1.string(from: val)
    }
    
    func getType (val: String) -> String {
        switch val{
        case "Water": return "(liters)"
        case "Sleep": return "Hrs"
        case "Running": return "Km"
        case "Cycling": return "Km"
        case "Steps": return ""
        default : return "(kcal)"
        }
    }
    
    func getDec(val: CGFloat) -> String {
        let format = NumberFormatter()
        format.numberStyle = .decimal
        
        return format.string(from: NSNumber.init(value: Float(val)))!
    }
    
    
    func getPercentage(current: CGFloat, Goal: CGFloat) -> String {
        let per = (current / Goal) * 100
        return String (format: "%.1f", per)
    }
    
    
    
}
