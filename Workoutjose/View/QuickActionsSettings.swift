//
//  QuickActionsSettings.swift
//  Workoutjose
//
//  Created by MacBook on 05.02.21.
//

import Foundation

class QuickActionSettings: ObservableObject {
    
    enum QuickAction: Hashable {
        case home
        case details(name: String)
    }
    
    @Published var quickAction: QuickAction? = nil
}
