//
//  TopMainView.swift
//  Workoutjose
//
//  Created by MacBook on 02.01.21.
//

import SwiftUI

struct TopMainView: View {
    
    
    @StateObject  var user = EditProfileMV()
    
    var body: some View {
        
        ZStack {
           
                
            
            HStack {
                HStack{
                    if let image = Image(user.imageName) {
                        image
                            .resizable()
                            .aspectRatio(contentMode: .fill)
                            .frame(width: 60, height: 60)
                            .clipShape(Circle())
                            .cornerRadius(50)
                           
                    }
                    
                    VStack(alignment: .leading) {
                        Text(user.name)
                            .font(.title3)
                            .fontWeight(.bold)
                            .foregroundColor(.white)
                            .padding(.bottom, 5.0)
                        
                        Text("Weight: " + "\(user.weight)")
                            .font(.caption)
                            .fontWeight(.medium)
                        
                        
                        
                        let bmi = bmiCalc(height: user.height, weight: user.weight)
                        
                        let bmiString = Text("Body Mass Index: " +  "\(bmi)")
                            .font(.caption)
                            .fontWeight(.semibold)
                        
                        
                        if bmi < 18.5 {
                            bmiString.foregroundColor(.red)
                        }
                        if bmi < 25.0 && bmi > 18.5 {
                            bmiString.foregroundColor(.green)
                        }
                        if bmi < 30.0 && bmi > 25.0 {
                            bmiString.foregroundColor(.yellow)
                        }
                        if bmi > 30 {
                            bmiString.foregroundColor(.red)
                        }
                        
                        
                    }
                    
                    
                    
                }.padding(15)
                Spacer()
                
                
                NavigationLink (
                    destination: EditProfileDataView(user: user),
                    label: {  Image("editIcon")
                        .resizable()
                        .frame(width: 80, height: 80, alignment: .center)
                        .scaledToFit()
                    }).frame(width: 80, height: 80, alignment: .center)
                
                
                
            }
            
            
            
            
        }
        .frame(height: 80)
        .background(LinearGradient(gradient: Gradient(colors: [Color.darkBlue, Color.white]), startPoint: .leading, endPoint: .trailing))
        .cornerRadius(9)
        
        
    }
}

struct TopMainView_Previews: PreviewProvider {
    static var previews: some View {
        TopMainView()
    }
}

