//
//  RectangleVideo.swift
//  Workoutjose
//
//  Created by MacBook on 29.01.21.
//

import SwiftUI
import AVFoundation
import AVKit


struct RectangleVideo: View {
    @EnvironmentObject var exerciseSetModel: ExerciseSetModel
    var body: some View {
        ZStack{
            WebVideoUnresizedView(player:PlayerUIView(frame: .zero, videoLink: exerciseSetModel.exerciseSets[ Int.random(in:0..<exerciseSetModel.exerciseSets.count)].link))
                .frame( height: 220).cornerRadius(9)
            Text("Workout Beast")
                .foregroundColor(.white)
                .font(.system(size: 30))
        }.shadow(radius: 10)
    }
}

