//
//  RoutinePreview.swift
//  Workoutjose
//
//  Created by MacBook on 03.02.21.
//

import SwiftUI

struct RoutinePreviewView: View {
    @EnvironmentObject var exerciseSetModel: ExerciseSetModel
    var routine: RoutineModel
    
    var body: some View {
        ZStack{
            WebVideoUnresizedView(player:PlayerUIView(frame: .zero, videoLink: exerciseSetModel.exerciseSets[ Int.random(in:0..<exerciseSetModel.exerciseSets.count)].link))
        VStack(spacing: UIScreen.main.bounds.height/18) {
            ZStack{
                RoundedRectangle(cornerRadius: 25, style: .continuous)
                    .fill(Color.white)
                    .shadow(radius: 10).frame(width: UIScreen.main.bounds.width/1.2, height: 100, alignment: .center)
                
                Text("Routine of " + getDate(val: routine.date)).foregroundColor(.darkBlue).bold().font(.title)
                
            }
            ScrollView(.horizontal, showsIndicators: false) {
                HStack( spacing: 15) {
                    ForEach(routine.exerciseTime) { exercise in
                        
                        CategoryItem(exercise: exercise.exercise)
                    }
                }.padding()
            }
            NavigationLink(destination: BeforePerformRoutineView(routine:  routine)) {
                ZStack{
                    Circle()
                        .fill(Color.darkBlue)
                        .frame(width: 150, height: 150)
                    Text("GO!").bold().foregroundColor(.white).font(.title).shadow(radius: 15)
                    
                } .shadow(radius: 15)
            }
            NavigationLink(
                destination: MainHomeView()
            ){
                FloatingMenu(image: "arrow.backward",height: 35,width: 35, system: true)
            }.padding()
            
            
        }
            
        }.frame(width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height+100).navigationBarTitle("")
        .navigationBarHidden(true)
        .navigationBarBackButtonHidden(true)
    }
    func getDate (val: Date) -> String {
        let formatter1 = DateFormatter()
        formatter1.dateStyle = .short
        return formatter1.string(from: val)
    }
}



struct RoutinePreviewView_Previews: PreviewProvider {
    
    static let modelData = ExerciseSetModel()
    static let exerciseTime = [
        ExerciseTime(id: 1, time: 30, exercise: modelData.exerciseSets[0]),
        ExerciseTime(id: 2, time: 30, exercise: modelData.exerciseSets[1]),
        ExerciseTime(id: 3, time: 30, exercise: modelData.exerciseSets[2]),
        ExerciseTime(id: 4, time: 30, exercise: modelData.exerciseSets[3])]
    static var previews: some View {
        let routine = RoutineModel(id: 1, date: Date(), exerciseTime:exerciseTime)
        
        RoutinePreviewView(routine: routine)
            .environmentObject(ExerciseSetModel())
    }
}
