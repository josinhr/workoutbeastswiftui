//
//  FloatingStadisticsView.swift
//  Workoutjose
//
//  Created by MacBook on 29.01.21.
//

import SwiftUI

struct FloatingMenu: View {
    var image: String?
    var height:CGFloat
    var width: CGFloat
    var system:Bool
    
    var body: some View {
        ZStack{
            Circle()
                .fill(Color.darkBlue)
                .frame(width: width*1.7, height: width*1.7)
            
            if(system){
                Image (systemName: image!)
                    .resizable()
                    .shadow(color: .gray, radius: 0.2, x: 1, y: 1)
                    .frame(width: width, height:height)
                    .shadow(radius: 10) .foregroundColor(.white)
            }
            else{
                Image (image!)
                    .resizable()
                    .clipShape(Circle())
                    .aspectRatio(contentMode: .fill)
                    .frame(width: width*1.7, height:height*1.7)
                    .shadow(radius: 10)
            }
        } .shadow(radius: 15)
    }
}
