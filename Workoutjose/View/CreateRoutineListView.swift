//
//  CreateRoutineView.swift
//  Workoutjose
//
//  Created by Alisafa Gasimov  on 1/24/21.
//

import SwiftUI


struct CreateRoutineListView: View {
 
   @EnvironmentObject var exerciseSetModel: ExerciseSetModel
   @ObservedObject var selectedExercise = RoutineRetrieveModel()

    var body: some View {

        ScrollView(){
        VStack(alignment: .center){
            
            ForEach(exerciseSetModel.categories.keys.sorted(), id: \.self) { key in
                CategoriesColumnView(categoryName: key, items: exerciseSetModel.categories[key]!, selectedExercise: selectedExercise
                )
            }.frame(height:UIScreen.main.bounds.height+200).padding()
        }
        }.padding(.top)
}


struct CreateRoutineListView_Previews: PreviewProvider {
    static var previews: some View {
        CreateRoutineListView().environmentObject(ExerciseSetModel())
    }
}

}

