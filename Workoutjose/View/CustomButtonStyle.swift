//
//  CustomButtonStyle.swift
//  Workoutjose
//
//  Created by Alisafa Gasimov  on 1/24/21.
//

import SwiftUI


struct CustomButtonStyle: ButtonStyle {
    var fillColor:Color = .darkBlue
    func makeBody(configuration: Configuration) -> some View {
        return CustomButton(configuration:configuration,
                            fillColor: fillColor)
    }
    struct CustomButton : View{
        let configuration: Configuration
        let fillColor :Color
        var body: some View {
            return configuration.label
                .padding(20)
                .background(RoundedRectangle(cornerRadius: 8).fill(fillColor))
        }
    }
    
}



