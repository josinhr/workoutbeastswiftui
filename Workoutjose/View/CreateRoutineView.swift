//
//  TopMainView.swift
//  Workoutjose
//
//  Created by MacBook on 02.01.21.
//

import SwiftUI

struct CreateRoutineView: View {
    @StateObject  var user = EditProfileMV()
    @State var isActiveRoutine :Bool = false
   
   
    var body: some View {
      
        NavigationLink(destination:  CreateRoutineListView() , isActive: $isActiveRoutine) {
                        HStack() {
                                Spacer()
                          Image(systemName:"plus.circle")
                          .font(.system(size: 24,weight:.semibold))
                              .foregroundColor(.white)
                            .frame( height: 80)
                                .scaledToFit()
                      
                          Text("Create a challenge")
                              .font(.system(size: 24,weight:.semibold))
                              .foregroundColor(.white)
                          Spacer()
                    }
                        .background(Color.darkBlue)
                        .cornerRadius(9)
                }

        }
    }


struct CreateRoutineView_Previews: PreviewProvider {
    static var previews: some View {
        CreateRoutineView()
    }
}

