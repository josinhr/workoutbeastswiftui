//
//  HeaderProfileView.swift
//  Workoutjose
//
//  Created by MacBook on 29.01.21.
//

import SwiftUI

struct Header: View {
    var body: some View {
        Rectangle()
            .frame(width: 500)
    }
}

struct ProfileImage: View {
    var image: String
    var body: some View {
        VStack {
            Image(image)
                .resizable()
                .clipShape(Circle())
                .overlay(Circle().stroke(Color.white, lineWidth: 4))
                .shadow(radius: 10)
                
        }
    }
}
